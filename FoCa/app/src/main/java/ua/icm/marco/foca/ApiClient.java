package ua.icm.marco.foca;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by Marco on 01/10/2017.
 */

public class ApiClient {

    private final String url;
    JSONArray menuOptionsArray, jsonArray2;
    JSONObject jsonObject;
    DailyOption dailyOption;
    SimpleDateFormat dateParser = new SimpleDateFormat("EEE, dd MMM yyyy", Locale.US);
    UAMenus menusToReturn;

    public ApiClient(String url) {
        this.url = url;
        this.menusToReturn = new UAMenus();
    }

    public interface UAMenusApiResponseListener {
        void handleRetrievedResults(UAMenus response);
    }

    public void getMenusForCanteen(Context context, final String selectedSite, final UAMenusApiResponseListener resultsHandler) {

        RequestQueue queue = Volley.newRequestQueue(context);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        parseJson(response);
                        UAMenus menus = parseJson(response);

                        if (null == selectedSite) {
                            menus.sortByCanteen();
                        } else {
                            menus.filterByCanteen(selectedSite);
                        }
                        resultsHandler.handleRetrievedResults(menus);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("FoCa", "Errors calling endpoint: ".concat(error.getLocalizedMessage()));
                    }
                });

        // Add the request to the RequestQueue.
        Log.i("FoCa", "request queued");
        queue.add(jsObjRequest);
    }


    public UAMenus parseJson(JSONObject response) {
        try {
            jsonObject = response.getJSONObject("menus");
            menuOptionsArray = jsonObject.getJSONArray("menu"); // zone

            // go through the several entries (day,canteen)
            for (int dailyEntry = 0; dailyEntry < menuOptionsArray.length(); dailyEntry++)
            {
                dailyOption = new DailyOption(
                        dateParser.parse(menuOptionsArray.getJSONObject(dailyEntry).getJSONObject("@attributes").getString("date")),
                        menuOptionsArray.getJSONObject(dailyEntry).getJSONObject("@attributes").getString("canteen"),
                        menuOptionsArray.getJSONObject(dailyEntry).getJSONObject("@attributes").getString("meal"),
                        menuOptionsArray.getJSONObject(dailyEntry).getJSONObject("@attributes").getString("disabled").compareTo("0") == 0);

                if (dailyOption.isAvailable()) {
                    jsonArray2 = menuOptionsArray.getJSONObject(dailyEntry).getJSONObject("items").getJSONArray("item");
                    // get the several meals in a canteen, in a day
                    dailyOption.addMealCourse(new Meal(UAMenus.COURSE_ORDER_SOUP, parseForObjectOrString(jsonArray2, UAMenus.COURSE_ORDER_SOUP)));
                    dailyOption.addMealCourse(new Meal(UAMenus.COURSE_ORDER_MEAT_NORMAL, parseForObjectOrString(jsonArray2, UAMenus.COURSE_ORDER_MEAT_NORMAL)));
                    dailyOption.addMealCourse(new Meal(UAMenus.COURSE_ORDER_FISH_NORMAL, parseForObjectOrString(jsonArray2, UAMenus.COURSE_ORDER_FISH_NORMAL)));
                    dailyOption.addMealCourse(new Meal(UAMenus.COURSE_ORDER_DIET, parseForObjectOrString(jsonArray2, UAMenus.COURSE_ORDER_DIET)));

                    dailyOption.addMealCourse(new Meal(UAMenus.COURSE_ORDER_VEGAN, parseForObjectOrString(jsonArray2, UAMenus.COURSE_ORDER_VEGAN)));
//                    dailyOption.addMealCourse(new Meal(UAMenus.COURSE_ORDER_OPTION, parseForObjectOrString(jsonArray2, UAMenus.COURSE_ORDER_OPTION)));
//                    dailyOption.addMealCourse(new Meal(UAMenus.COURSE_ORDER_SALAD, parseForObjectOrString(jsonArray2, UAMenus.COURSE_ORDER_SALAD)));
//                    dailyOption.addMealCourse(new Meal(UAMenus.COURSE_ORDER_MISC, parseForObjectOrString(jsonArray2, UAMenus.COURSE_ORDER_MISC)));
//                    dailyOption.addMealCourse(new Meal(UAMenus.COURSE_ORDER_DESSERT, parseForObjectOrString(jsonArray2, UAMenus.COURSE_ORDER_DESSERT)));
                    //// TODO: complete with more meal courses


                }
                menusToReturn.add(dailyOption);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return menusToReturn;
    }

    public String parseForObjectOrString(JSONArray array, int index) throws JSONException {
        JSONObject tempJsonObject = array.optJSONObject(index);
        if (null == tempJsonObject) {
            // no json object, treat as string
            return array.getString(index);
        } else {
            return array.getJSONObject(index).getJSONObject("@attributes").getString("name");
        }
    }
}
