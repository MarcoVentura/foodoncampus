package ua.icm.marco.foca;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Marco on 01/10/2017.
 */

public class DailyOption {
    static final public int DAILY_MEAL_LUNCH = 1;
    static final public int DAILY_MEAL_DINNER = 2;

    private Date date;
    private String canteenSite;
    private String dailyMeal;
    private boolean available;
    private List<Meal> mealCourseList;

    public DailyOption(Date date, String canteen, String dailyMeal, boolean available) {
        this.canteenSite = canteen;
        this.date = date;
        this.dailyMeal = dailyMeal;
        this.available = available;
        mealCourseList = new ArrayList<>();
    }

    public void addMealCourse(Meal meal) {
        mealCourseList.add(meal);
    }

    public List<Meal> getMealCourseList() {
        return mealCourseList;
    }

    public Date getDate() {
        return date;
    }

    public String getCanteenSite() {
        return canteenSite;
    }

    public String getDailyMeal() {
        return dailyMeal;
    }

    public boolean isAvailable() {
        return available;
    }

    @Override
    public String toString() {
        return "DailyOption{" +
                " " +  parseDate() +
                ", canteenSite='" + canteenSite + '\'' +
                ", dailyMeal='" + dailyMeal + '\'' +
                ", available=" + available +
                " \nmealCourseList=" + mealCourseList +
                '}';
    }

    public String parseDate() {
        String nDate = new SimpleDateFormat("dd/MM/yyyy").format(date);
        return nDate;
    }
}
