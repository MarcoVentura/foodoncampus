package ua.icm.marco.foca;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Created by Marco on 02/10/2017.
 */

public class MealListView extends ArrayAdapter<String>
{
    private String [] dia;
    private String [] meal;
    private boolean [] available;
    private String [] sopa;
    private String [] mealOne;
    private String [] mealTwo;
    private Activity context;

    public MealListView(Activity context, String[] dia, String [] meal, boolean [] available,
                        String [] sopa, String [] mealOne, String [] mealTwo) {
        super(context, R.layout.meal_list, meal);

        this.context = context;
        this.dia = dia;
        this.meal = meal;
        this.available = available;
        this.sopa = sopa;
        this.mealOne = mealOne;
        this.mealTwo = mealTwo;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View r = convertView;
        ViewHolder2 viewHolder = null;

        if (r == null) {
            LayoutInflater layoutInflater = context.getLayoutInflater();
            r = layoutInflater.inflate(R.layout.meal_list, null, true);
            viewHolder = new ViewHolder2(r);
            r.setTag(viewHolder);
        } else
            viewHolder = (ViewHolder2) r.getTag();

        if (dia.length > position) {
        viewHolder.textView.setText(dia[position]);
        }
        if (sopa.length > position) {
            viewHolder.textView2.setText(sopa[position]);
        }
        if (mealOne.length > position) {
            viewHolder.textView3.setText(mealOne[position]);
        }
        if (mealTwo.length > position) {
            viewHolder.textView4.setText(mealTwo[position]);
        }
        if (meal.length > position) {
            viewHolder.textView6.setText(meal[position]);
        }
        if (available[position] == true) {
            viewHolder.textView7.setText("Aberto");
        }
        else {
            viewHolder.textView7.setText("Fechado");
        }

        return r;
    }

    class ViewHolder2 {
        TextView textView;
        TextView textView2;
        TextView textView3;
        TextView textView4;
        TextView textView6;
        TextView textView7;

        ViewHolder2(View v) {
            textView = (TextView) v.findViewById(R.id.diaID);
            textView2 = (TextView) v.findViewById(R.id.sopa);
            textView3 = (TextView) v.findViewById(R.id.mealOne);
            textView4 = (TextView) v.findViewById(R.id.mealTwo);
            textView6 = (TextView) v.findViewById(R.id.mealDay);
            textView7 = (TextView) v.findViewById(R.id.openID);

        }
    }
}
