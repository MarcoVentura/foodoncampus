package ua.icm.marco.foca;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Marco on 01/10/2017.
 */

public class CustomListView extends ArrayAdapter<String>{

    private String [] cantinas;
    private Integer [] imagens;
    private Activity context;

    public CustomListView(Activity context, String [] cantinas, Integer [] imagens) {
        super(context, R.layout.list_item, cantinas);
        this.context = context;
        this.cantinas = cantinas;
        this.imagens = imagens;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View r = convertView;
        ViewHolder viewHolder = null;

        if (r == null) {
            LayoutInflater layoutInflater = context.getLayoutInflater();
            r = layoutInflater.inflate(R.layout.list_item, null, true);
            viewHolder = new ViewHolder(r);
            r.setTag(viewHolder);
        }
        else
            viewHolder = (ViewHolder) r.getTag();

        viewHolder.imageView.setImageResource(imagens[position]);
        viewHolder.textView.setText(cantinas[position]);
        return r;
    }

    class ViewHolder {
        TextView textView;
        ImageView imageView;

        ViewHolder(View v) {
            textView = (TextView) v.findViewById(R.id.cantinaName);
            imageView = (ImageView) v.findViewById(R.id.cantinaImage);
        }
    }
}
