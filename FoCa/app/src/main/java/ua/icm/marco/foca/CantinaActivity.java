package ua.icm.marco.foca;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;

public class CantinaActivity extends FragmentActivity {

    private MealListView mealListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cantina);

        String item = (String) getIntent().getSerializableExtra("item");

        Bundle extras = getIntent().getExtras();
        String [] dia = extras.getStringArray("dia");
        String [] pratoCarne = extras.getStringArray("carne");
        String [] pratoPeixe = extras.getStringArray("peixe");
        boolean [] available = extras.getBooleanArray("avail");
        String [] meal = extras.getStringArray("meal");
        String [] sopa = extras.getStringArray("sopa");

        if (savedInstanceState == null) {
            MealListFragment fragmentItem = MealListFragment.newInstance(extras);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.flDetailContainer, fragmentItem);
            ft.commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
}