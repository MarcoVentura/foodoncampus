package ua.icm.marco.foca;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

/**
 * Created by Marco on 01/10/2017.
 */

public class UAMenus {
    static final public int COURSE_ORDER_SOUP = 0;
    static final public int COURSE_ORDER_MEAT_NORMAL = 1;
    static final public int COURSE_ORDER_FISH_NORMAL = 2;
    static final public int COURSE_ORDER_DIET = 3;
    static final public int COURSE_ORDER_VEGAN = 4;
    static final public int COURSE_ORDER_OPTION = 5;
    static final public int COURSE_ORDER_SALAD = 6;
    static final public int COURSE_ORDER_MISC = 7;
    static final public int COURSE_ORDER_DESSERT = 8;


    private List<DailyOption> dailyMenusPerCanteen = new ArrayList<>();

    public void add(DailyOption dailyOption) {
        this.getDailyMenusPerCanteen().add(dailyOption);
    }

    /**
     * dumps the content of the object into a string for logging/dubbuging
     * @return the contents as String
     */
    public String formatedContentsForDebugging() {
        SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        StringBuilder builder = new StringBuilder();
        for (DailyOption option : this.getDailyMenusPerCanteen()) {
            builder.append("\nDay: ");
            builder.append(dateFormater.format(option.getDate()));
            builder.append("\nCanteen: ");
            builder.append(option.getCanteenSite());
            builder.append("\nMeal type: ");
            builder.append(option.getDailyMeal());
            builder.append("\nIs open? ");
            builder.append(option.isAvailable());
            builder.append("\n");
            for (Meal mealOption: option.getMealCourseList() ) {
                builder.append("\nCourse: ");   builder.append(mealOption.getMealCourseOrder());
                builder.append("\nmeal: ");   builder.append(mealOption.getFoodOptionDescription());
                builder.append("\n");
            }
        }
        return builder.toString();
    }

    public List<DailyOption> getDailyMenusPerCanteen() {
        return dailyMenusPerCanteen;
    }

    public void filterByCanteen(String selectedSite) {
        List<DailyOption> filtered = new ArrayList<>();
        for ( DailyOption entry: getDailyMenusPerCanteen()  ) {
            if( entry.getCanteenSite().compareToIgnoreCase( selectedSite) == 0) {
                filtered.add(entry);
            }
        }
        dailyMenusPerCanteen = filtered;
    }

    public void sortByCanteen() {
        Collections.sort(dailyMenusPerCanteen, new Comparator<DailyOption>() {
            @Override
            public int compare(DailyOption do1, DailyOption do2) {
                int same = do1.getCanteenSite().compareTo(do2.getCanteenSite());
                if (same == 0) {
                    return do1.getDate().compareTo(do2.getDate());

                }
                return same;

            }
        });
    }
}
