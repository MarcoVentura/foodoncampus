package ua.icm.marco.foca;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

/**
 * Created by Marco on 02/10/2017.
 */

public class MealListFragment extends Fragment {

    private ListView mealList;
    private MealListView mealListView;
    private String[] dia;
    private boolean[] available;
    private String[] meal, pratoCarne, pratoPeixe, sopa;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dia = (String[]) getArguments().getSerializable("dia");
        pratoCarne = (String[]) getArguments().getSerializable("carne");
        pratoPeixe = (String[]) getArguments().getSerializable("peixe");
        available = (boolean[]) getArguments().getSerializable("avail");
        meal = (String[]) getArguments().getSerializable("meal");
        sopa = (String[]) getArguments().getSerializable("sopa");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_cantina_detail,
                container, false);

        mealList = (ListView) view.findViewById(R.id.mealListID);

        int count;

        for (int i = 0; i < dia.length; i++)
        {
            System.out.println(dia[i]);
        }

        for (int i = 0; i < pratoCarne.length; i++)
        {
            System.out.println(pratoCarne[i]);
        }

        for (int i = 0; i < meal.length; i++)
        {
            System.out.println(meal[i]);
        }

        mealListView = new MealListView(getActivity(), dia, meal, available, sopa, pratoCarne, pratoPeixe);

        mealList.setAdapter(mealListView);
        return view;
    }

    // ItemDetailFragment.newInstance(item)
    public static MealListFragment newInstance(Bundle extras) {
        MealListFragment fragmentDemo = new MealListFragment();
        Bundle args = new Bundle();
        args.putSerializable("dia", extras.getStringArray("dia"));
        args.putSerializable("carne", extras.getStringArray("carne"));
        args.putSerializable("peixe", extras.getStringArray("peixe"));
        args.putSerializable("meal", extras.getStringArray("meal"));
        args.putSerializable("sopa", extras.getStringArray("sopa"));
        args.putSerializable("avail", extras.getBooleanArray("avail"));

        fragmentDemo.setArguments(args);
        return fragmentDemo;
    }
}
