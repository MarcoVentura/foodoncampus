package ua.icm.marco.foca;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.widget.FrameLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends FragmentActivity implements CantinaListFragment.OnItemSelectedListener {

    private boolean isTwoPane = false;

    private ApiClient cliente;
    private String [] dia;
    private boolean [] available;
    private String [] meal, pratoCarne, pratoPeixe, sopa;
    private List<DailyOption> listMeals;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        determinePaneLayout();
    }

    private void determinePaneLayout() {
        FrameLayout fragmentItemDetail = (FrameLayout) findViewById(R.id.flDetailContainer);
        if (fragmentItemDetail != null) {
            isTwoPane = true;
            CantinaListFragment fragmentItemsList =
                    (CantinaListFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentItemsList);
            fragmentItemsList.setActivateOnItemClick(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onItemSelected(String selectedSite) {
            changeActivity(selectedSite);
        }

    public void changeActivity(String selectedSite)
    {
        String url = this.getString(R.string.api_endpoint);
        if (cliente == null) {
            cliente = new ApiClient(url);
        }

        cliente.getMenusForCanteen(this, selectedSite, new ApiClient.UAMenusApiResponseListener() {

            @Override
            public void handleRetrievedResults(UAMenus response) {
                Log.i("FoCa", response.formatedContentsForDebugging());

                doSomething(response);
            }
        });
    }

    public void doSomething(UAMenus menus) {
        listMeals = menus.getDailyMenusPerCanteen();
        List<String> listaDias = new ArrayList<>();
        List<String> listarefeicao = new ArrayList<>();
        List<Boolean> listaAberto = new ArrayList<>();
        List<String> listaSopas = new ArrayList<>();
        List<String> listaPratoCarne = new ArrayList<>();
        List<String> listaPratoPeixe = new ArrayList<>();
        String nDate;

        for (DailyOption d : listMeals) {
            nDate = new SimpleDateFormat("dd/MM/yyyy").format(d.getDate());
            listaDias.add(nDate);

            listarefeicao.add(d.getDailyMeal());
            listaAberto.add(d.isAvailable());

            if(d.isAvailable()) {
                System.out.println("meal 0 : " + d.getMealCourseList().get(0).getFoodOptionDescription());
                System.out.println("meal 1 : " + d.getMealCourseList().get(1).getFoodOptionDescription());

                listaSopas.add(d.getMealCourseList().get(0).getFoodOptionDescription());
                listaPratoCarne.add(d.getMealCourseList().get(1).getFoodOptionDescription());
                listaPratoPeixe.add(d.getMealCourseList().get(2).getFoodOptionDescription());
            }
        }

        dia = listaDias.toArray(new String[0]);
        available = new boolean[listaAberto.size()];
        for(int n = 0; n < listaAberto.size(); n++) {
            available[n] = listaAberto.get(n);
        }
        sopa = listaSopas.toArray(new String[0]);
        pratoCarne = listaPratoCarne.toArray(new String[0]);
        pratoPeixe = listaPratoPeixe.toArray(new String[0]);
        meal = listarefeicao.toArray(new String[0]);

        Bundle extras = new Bundle();
        extras.putStringArray("dia", dia);
        extras.putStringArray("carne", pratoCarne);
        extras.putBooleanArray("avail", available);
        extras.putStringArray("meal", meal);
        extras.putStringArray("peixe", pratoPeixe);
        extras.putStringArray("sopa", sopa);


        if (isTwoPane) { // single activity with list and detail
            MealListFragment fragmentItem = MealListFragment.newInstance(extras);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.flDetailContainer, fragmentItem);
            ft.commit();
        }
        else
        {
            Intent i = new Intent(this, CantinaActivity.class);
            i.putExtras(extras);
            startActivity(i);
        }
    }
}
