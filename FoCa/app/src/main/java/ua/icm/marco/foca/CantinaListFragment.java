package ua.icm.marco.foca;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

/**
 * Created by Marco on 02/10/2017.
 */

public class CantinaListFragment extends Fragment {

    private ListView listView;    ;
    private String selectedSite = null;
    Integer []imagesID = {R.drawable.santiago, R.drawable.crasto, R.drawable.snackbar};
    String [] nomes;
    private CustomListView customListView;

    private OnItemSelectedListener listener;

    public interface OnItemSelectedListener {
        public void onItemSelected(String s);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnItemSelectedListener) {
            listener = (OnItemSelectedListener) context;
        } else {
            throw new ClassCastException(
                    context.toString()
                            + " must implement ItemsListFragment.OnListItemSelectedListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate view
        View view = inflater.inflate(R.layout.fragment_cantina_list, container,
                false);

        nomes = getResources().getStringArray(R.array.cantinas);
        listView = (ListView) view.findViewById(R.id.cantinasID);
        customListView = new CustomListView(getActivity(), nomes, imagesID);

        listView.setAdapter(customListView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View item, int position,
                                    long rowId) {
                // Retrieve item based on position
                selectedSite = customListView.getItem(position);
                System.out.println(selectedSite);
                listener.onItemSelected(selectedSite);
            }
        });
        return view;
    }

    /**
     * Turns on activate-on-click mode. When this mode is on, list items will be
     * given the 'activated' state when touched.
     */
    public void setActivateOnItemClick(boolean activateOnItemClick) {
        // When setting CHOICE_MODE_SINGLE, ListView will automatically
        // give items the 'activated' state when touched.
        listView.setChoiceMode(
                activateOnItemClick ? ListView.CHOICE_MODE_SINGLE
                        : ListView.CHOICE_MODE_NONE);
    }

}
